import React from 'react';
import {Col, Panel, Glyphicon} from 'react-bootstrap';
import love from '../love.png';

const Post = (props) => {
    return (
        <Col md={4} className="card">
            <Panel bsClass="main-panel">
                <Panel.Body bsClass="panel">
                    <img 
                    className="img-size" 
                    alt="posts-img"
                    src={props.image}/>
        
                    <div className="overlay" 
                        onClick={()=>props.showModel(props.id)}>
                    <span className="overlay-text">
                        <img alt="likes"
                        src={love} 
                        className="img-icon"/> 
                        <b>{props.likes}</b>
                        
                        <span className="comment-ico">
                        <Glyphicon 
                            glyph="comment"/>
                         <b>{props.comments}</b>
                        </span>
                    </span>
                    </div>  
                </Panel.Body>
            </Panel>
        </Col>
        )
    
  }

export default Post